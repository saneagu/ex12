package ro.orangeStartIT;

public class TestGenTypes {

    public static void main(String[] args) {
        //Create one instantiation for the GenTypes class
        GenTypes<String,Integer> Employee = new GenTypes<>("David Gilbert",1700);

//        GenTypes<String> Employee = new GenTypes<>("David Gilbert");
//        GenTypes<Integer> Salary = new GenTypes<>(1700);

        System.out.println("Employee: " + Employee.getname() + "\n" + "Salary: " + Employee.getvalue() + " dollars");
    }
}
