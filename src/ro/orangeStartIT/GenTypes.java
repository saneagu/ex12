package ro.orangeStartIT;
    //A public class with two parameters named T1 and T2
public class GenTypes<T1, T2> {
    public T1 name;
    public T2 value;

    //Two objects for T1 and T2 named : name and value
    GenTypes(T1 name, T2 value){
        this.name = name;
        this.value = value;
        }
    //Getters methods for these two objects
    public T1 getname() {
        return name;
        }

    public T2 getvalue() {
        return value;
        }

    }
